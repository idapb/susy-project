from numpy import *
import matplotlib.pyplot as plt

mass = []
file = open('neutralinos.txt','r')
for line in file:
    values = line.split()
    for value in values:
        mass.append(float(value))

chi01 = zeros(len(mass)/4)
chi02 = zeros(len(mass)/4)
chi03 = zeros(len(mass)/4)
chi04 = zeros(len(mass)/4)

for i in range(int((len(mass))/4.)):
    chi01[i] = mass[i*4]
    chi02[i] = mass[1+i*4]
    chi03[i] = mass[2+i*4]
    chi04[i] = mass[3+i*4]

mass2 = []
file = open('charginos.txt','r')
for line in file:
    values = line.split()
    for value in values:
        mass2.append(float(value))

chipm1 = zeros(len(mass2)/4)
chipm2 = zeros(len(mass2)/4)
chipm3 = zeros(len(mass2)/4)
chipm4 = zeros(len(mass2)/4)

for i in range(int((len(mass2))/4.)):
    chipm1[i] = mass2[i*4]
    chipm2[i] = mass2[1+i*4]
    chipm3[i] = mass2[2+i*4]
    chipm4[i] = mass2[3+i*4]

M1 = []
file = open('M1.txt','r')
for line in file:
    values = line.split()
    for value in values:
        M1.append(float(value))
M1 = array(M1)

M2 = []
file = open('M2.txt','r')
for line in file:
    values = line.split()
    for value in values:
        M2.append(float(value))
M2 = array(M2)

x = []
file = open('x.txt','r')
for line in file:
    values = line.split()
    for value in values:
        x.append(float(value))
x = array(x)

plt.plot((x),abs(chi01),label=r'$m_{\tilde{\chi}^0_1}$')
plt.plot((x),abs(chi02),label=r'$m_{\tilde{\chi}^0_2}$')
plt.plot((x),abs(chi03),label=r'$m_{\tilde{\chi}^0_3}$')
plt.plot((x),abs(chi04),label=r'$m_{\tilde{\chi}^0_4}$')
plt.xlabel(r'$M_1$',fontsize=17)
plt.ylabel('Mass [GeV]')
plt.title('Mass of neutralinos and charginos')
#plt.axis([3,50,0,120])
plt.text(30,40,r'$M_2 = 500$, $\mu = 1000$',size=15,
         bbox=dict(boxstyle='square', facecolor='white'))

plt.plot((x),abs(chipm1), '--',color='black',label=r'$m_{\tilde{\chi}^{\pm}_1}$')
plt.plot((x),abs(chipm3), '--',label=r'$m_{\tilde{\chi}^{\pm}_2}$')
plt.legend(fontsize=17)
#plt.savefig('mass(tan(beta)).pdf', bbox='tight')
plt.show()
