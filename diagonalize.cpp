#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <armadillo>
#include <complex>

using namespace std;
using namespace arma;

typedef complex<double> cx_double;
typedef Mat<cx_double> cx_mat;
typedef Col<cx_double> cx_vec;

double thetaW = asin(sqrt(0.23));
double mW = 80.385;
double mZ = 91.1876;

vec neutralinos(cx_double M1, cx_double M2, cx_double mu, double beta){

  //creating neutralino mass matrix
  mat A = zeros(4,4);
  mat B = zeros(4,4);
  cx_mat neutralinoMatrix(A,B);
  neutralinoMatrix(0,0) = M1;
  neutralinoMatrix(1,1) = M2;
  neutralinoMatrix(2,3) = neutralinoMatrix(3,2) = -mu;
  neutralinoMatrix(2,0) = neutralinoMatrix(0,2) = cx_double (-cos(beta)*sin(thetaW)*mZ,0);
  neutralinoMatrix(3,0) = neutralinoMatrix(0,3) = cx_double(sin(beta)*sin(thetaW)*mZ,0);
  neutralinoMatrix(3,1) = neutralinoMatrix(1,3) = cx_double(-sin(beta)*cos(thetaW)*mZ,0);
  neutralinoMatrix(2,1) = neutralinoMatrix(1,2) = cx_double(cos(beta)*cos(thetaW)*mZ,0);
  //finding eigenvalues for complex, symmetric matrix
  vec eigval;
  eig_sym(eigval, neutralinoMatrix);

  return eigval;
}

vec charginos(cx_double M2, cx_double mu, double beta){

  //creating chargino mass matrix
  mat A = zeros(4,4);
  mat B = zeros(4,4);
  cx_mat charginoMatrix(A,B);
  charginoMatrix(2,0) = charginoMatrix(0,2) = M2;
  charginoMatrix(3,0) = charginoMatrix(0,3) = cx_double (sqrt(2)*cos(beta)*mW,0);
  charginoMatrix(2,1) = charginoMatrix(1,2) = cx_double (sqrt(2)*sin(beta)*mW,0);
  charginoMatrix(1,3) = charginoMatrix(3,1) = mu;

  //finding eigenvalues for complex, symmetric matrix
  vec eigval;
  eig_sym(eigval, charginoMatrix);

  return eigval;
}

int main(){

  vec temp_real = zeros(300);
  for(int i=0;i<300;i++){
    temp_real(i) = i;
  }
  vec temp_imag = zeros(300);
  cx_vec mu (temp_real,temp_imag);
  //cx_double M1 (300,0); 
  double beta = atan(2); 
  cx_double M1 (15,0);  
  cx_double M2 (10,0);  
  //vec beta = atan(temp_real);

  ofstream myfile;
  myfile.open("neutralinos.txt");
  ofstream myfile3;
  myfile3.open("charginos.txt");
  ofstream myfile4;
  myfile4.open("x.txt");

  for(int i=0;i<300;i++){
    vec neutralinoMasses = neutralinos(M1,M2,mu(i),beta);
    sort (neutralinoMasses.begin(), neutralinoMasses.end(), [](int j, int k) { return abs(j) < abs(k); });
    myfile << neutralinoMasses(0) << " " << neutralinoMasses(1) << " " << neutralinoMasses(2) << " " << neutralinoMasses(3) << " ";
    myfile4 << mu(i).real() << " ";
  }

  for(int i=0;i<300;i++){
    vec charginoMasses = charginos(M2,mu(i),beta);
    sort (charginoMasses.begin(), charginoMasses.end(), [](int j, int k) { return abs(j) < abs(k); });
    myfile3 << charginoMasses(0) << " " << charginoMasses(1) << " " << charginoMasses(2) << " " << charginoMasses(3) << " ";
  }

  myfile.close();
  myfile3.close();
  myfile4.close();

//  //Find lightest chargino for sum of two lightest neutralinos below 90 GeV
//
//  ofstream myfile;
//  myfile.open("M1.txt");
//  ofstream myfile2;
//  myfile2.open("M2.txt");
//  ofstream myfile3;
//  myfile3.open("tanBeta.txt");
//  ofstream myfile4;
//  myfile4.open("chargino.txt");
//  ofstream myfile5;
//  myfile5.open("mu.txt");
//
//  vec M1_real = zeros(50);
//  vec M1_imag = zeros(50);
//  vec M2_real = zeros(50);
//  vec M2_imag = zeros(50);
//  vec betaTemp = zeros(50);
//  vec mu_imag = zeros(50);
//  vec mu_real = zeros(50);
//  for(int i=0;i<50;i++){
//    M1_real(i) = i;
//    M2_real(i) = 80+i;
//    mu_real(i) = 180+i;
//  }
//  for(int i=0;i<30;i++){
//    betaTemp(i) = i+2;
//  }
//  vec beta = atan(betaTemp);
//  //double beta = atan(10);
//  cx_vec M1 (M1_real,M1_imag);
//  cx_vec M2 (M2_real,M2_imag);
//  cx_vec mu (mu_real,mu_imag);
//
//  for(int i=0;i<50;i++){
//    for(int j=0;j<50;j++){
//      for(int k=0;k<50;k++){
//	for(int n=0;n<30;n++){
//	  vec neutralinoMasses = neutralinos(M1(i),M2(j),mu(k),beta(n));
//	  vec charginoMasses = charginos(M2(j),mu(k),beta(n));
//	  sort (neutralinoMasses.begin(), neutralinoMasses.end(), [](int l, int m) { return abs(l) < abs(m); });
//	  if((abs(neutralinoMasses(0))+abs(neutralinoMasses(1))) < 90){
//	    myfile << M1(i).real() << " ";
//	    myfile2 << M2(j).real() << " ";
//	    myfile3 << beta(n) << " ";
//	    myfile5 << mu(k).real() << " ";
//	    myfile4 << min(abs(charginoMasses)) << " ";
//	  }    
//	}
//      }
//    }
//  }  
//  
//  myfile.close();
//  myfile2.close();
//  myfile3.close();
//  myfile4.close();
//  myfile5.close();
  
  return 0;
}

