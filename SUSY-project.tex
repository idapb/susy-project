\documentclass[twocolumn,a4paper,10pt]{article}

%\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{graphicx}
%\usepackage{xcolor}
%\hypersetup{
%    colorlinks,
%    linkcolor={red!50!black},
%    citecolor={blue!50!black},
%    urlcolor={blue!80!black}
%}

% Variable enumeration
\usepackage{enumerate}

% Use all allowed space
\addtolength{\hoffset}{-0.5cm}
\addtolength{\textwidth}{1.0cm}
\addtolength{\voffset}{-1.5cm}
\addtolength{\textheight}{3cm}
\setlength{\columnsep}{0.5cm}

% Remove Abstract titile for abstract
%\renewcommand{\abstractname}{}

\author{Ida Pauline Buran}

\title{Mass Mixing Matrix Mayhem in the MSSM}

\begin{document}

% Abstract construction for using both columns in two-column format
\twocolumn[
\begin{@twocolumnfalse}
  \maketitle
  \begin{abstract}
In this project we study the masses of the neutralinos and the charginos in the minimal supersymmetric standard model (MSSM). The neutralinos and charginos are formed from the mixing of neutral and charged fermion fields respectively, and the masses of these particles are given by mass matrices found from the Lagrangian. We write a computer program which can find the mass eigenvalues of the matrices, and using this program we investigate what the mass spectrum and neutralino-chargino composition looks like in the limits where one mass parameter is smaller than the others: $M_1 << M_2$ and $\mu$ (bino LSP), $M_2 << M_1$ and $\mu$ (wino LSP), $\mu << M_1$ and $M_2$ (higgsino LSP). We also check how heavy the lightest chargino can be if the sum of the two lightest neutralinos is below 90 Gev.

 \end{abstract}
  \vspace{5mm}
\end{@twocolumnfalse}
]

\section{Introduction}
The Standard Model (SM) is the most successful theory of particle physics to date. It classifies all the known subatomic particles and explains how they interact through the electromagnetic, weak and strong nuclear forces. The SM has succesfully explained almost all experimental results and precisely predicted a wide variety of phenomena. It is, however, an incomplete theory. There are fundamental physical phenomena the SM can not explain. One particular example is that the square of the Higgs mass in the SM gets enormous quantum corrections from every particle that couple to the Higgs field. For the Higgs mass to be close to the measured value, there must be an incredible fine tuning which cancels these quantum corrections. This is known as the ``hierarchy problem''.
Another example is that most of the matter in the universe is dark matter, which is not explained by the SM.
Several theories describing new physics beyond the SM have been created in attempt to explain these SM deficiencies, one of the most popular being supersymmetry (SUSY) at the TeV scale with R-parity conservation, for a review see \cite{Martin:1997ns}.

Susy introduces a symmetry between fermionic and bosonic fields. This leads to a whole new set of particles, superpartners of the known SM particles, that differ from the SM particles only by spin one half. Since no supersymmetric particles have been discovered yet, this must mean that if supersymmetry exists it has to be a broken symmetry, making the superpartners much heavier than their SM partners. If SUSY is to solve the SM deficiencies however, the particles must be close to the TeV scale. For example for the hierarchy problem, the quantum corrections from the SUSY particles, if they are close to 1 TeV, will cancel the ones from the SM particles making the observed Higgs mass less fine tuned.

Several SUSY models introduce the concept of R-parity to deal with problems such as proton decay, which is made possible by the supersymmetric Lagrangian. All SM particles then have even R-parity and SUSY particles have odd R-parity. This means that if R-parity is conserved, all supersymmetric particles are produced in pairs, and they cascade decay down to the lightest supersymmetric particle (LSP) which is stable since it can not decay to a SM particle. 

The Minimal Supersymmetric Standard Model (MSSM) has the smallest possible field and gauge content consistent with the SM, and is the one which will be considered here. In the MSSM the LSP is typically the lightest neutralino, which is electrically neutral and weakly interacting, and therefore a good candidate for dark matter. If it was electrically charged or strongly interacting it would have been discovered already. 

\section{Neutralinos and Charginos}
In the MSSM there are several fermion fields that can mix to create several new particles.
These fields are the superpartners of the $SU(2)\times U(1)$ gauge fields: 
\[
\tilde{B}^0, \tilde{W}^0\:\text{and}\: \tilde{W}^{\pm}
\]
 and the scalar Higgs fields: 
\[
\tilde{H}^+_u, \tilde{H}^0_u, \tilde{H}^-_d\:\text{and}\: \tilde{H}^0_d.
\]
Electroweak symmetry is broken so we do not have to consider $SU(2)_L\times U(1)_Y$ charges when mixing the fields, only the $U(1)_{em}$ charges matter. 
This gives four mass eigenstates from the mixing of the four neutral fields, called neutralinos, $\tilde{\chi}^0_i$, where $i=1,2,3,4$, and four mass eigenstates from the mixing of the four charged fields, called charginos, $\tilde{\chi}^{\pm}_i$, where i = 1,2.\\\\
We can write the mixing of the neutralinos in the gauge eigenstate basis:
\[
\tilde{\psi}^{0T} = (\tilde{B}^0, \tilde{W}^0, \tilde{H}^0_d, \tilde{H}^0_u)
\]
as:
\[
\tilde{\chi}^0_i = N_{i1}\tilde{B}_0 + N_{i2}\tilde{W}^{0} + N_{i3}\tilde{H}^0_d + N_{i4}\tilde{H}^0_u,
\]
where $N_{ij}$ is the size of the component of each field. In the same basis we can write the neutralino mass term as:
\[
\mathcal{L}_{\tilde{\chi}^0_\text{mass}} = -\frac{1}{2}\tilde{\psi}^{0T}M_{\tilde{\chi}^0}\tilde{\psi}^0 + c.c.
\]
where 
\[
M_{\tilde{\chi}^0} = 
\begin{bmatrix} 
M_1 & 0 & -\frac{1}{\sqrt{2}}g'v_d & \frac{1}{\sqrt{2}}g'v_u \\
0 & M_2 & \frac{1}{\sqrt{2}}gv_d & -\frac{1}{\sqrt{2}}gv_u \\
-\frac{1}{\sqrt{2}}g'v_d & \frac{1}{\sqrt{2}}gv_d & 0 & -\mu \\
\frac{1}{\sqrt{2}}g'v_u & -\frac{1}{\sqrt{2}}gv_u & -\mu & 0
\end{bmatrix}
\]
The $M_1$ term in this matrix is from the soft term for the $\tilde{B}^0$ in the Lagrangian and the $M_2$ term is from the soft term for the $\tilde{W}^0$, the $\mu$ terms are from the superpotential term $\mu H_uH_d$, and the rest is from the Higgs-higgsino-gaugino terms from the kinetic part of the Lagrangian.

There is an experimental electroweak constraint on the Higgs vev's which needs to be fulfilled to get the familiar vector boson masses:
\[
v_u^2 + v_d^2 = \frac{2m_Z^2}{g^2+g'^2} \approx (174 GeV)^2
\]
This gives one free parameter coming from the Higgs vev's which can be written as:
\[
\tan\beta \equiv \frac{v_u}{v_d},
\]
where $0 < \beta < \pi/2$. Using this condition we can rewrite the Higgs-higgsino-gaugino terms in the mass matrix in terms of $\beta$, the Weinberg angle, $\theta_W$, and the Z-mass. This gives:
\[
\frac{1}{\sqrt{2}}g'v_d = \cos\beta\sin\theta_Wm_Z,
\]
\[
\frac{1}{\sqrt{2}}g'v_u = \sin\beta\sin\theta_Wm_Z,
\]
\[
\frac{1}{\sqrt{2}}gv_d = \cos\beta\cos\theta_Wm_Z,
\]
\[
\frac{1}{\sqrt{2}}gv_u = \sin\beta\cos\theta_Wm_Z.
\]
We can diagonalize the mass matrix to find the masses of the neutralinos. The mass eigenstates are $\tilde{\chi}^0_i = N_{ij}\tilde{\psi}^0_j$, where $N$ is the matrix that diagonalizes $M_{\tilde{\chi}^0}$ so that $NM_{\tilde{\chi}^0}N^{-1} = D$, where $D = (m_{\tilde{\chi}^0_1},m_{\tilde{\chi}^0_2},m_{\tilde{\chi}^0_3},m_{\tilde{\chi}^0_4})$ is the diagonal matrix containing the neutralino masses. The neutralinos are ordered so that the lightest particle has the lowest index. The mass eigenvalues can be negative or even complex, but this is not a problem as a phase factor can be absorbed into $N$ leaving the masses real and positive.\\\\
From the charged fermion fields we get the charginos, $\tilde{\chi}^{\pm}$, with mass term:
\[
\mathcal{L}_{\tilde{\chi}^{\pm}_\text{mass}} = -\frac{1}{2}\tilde{\psi}^{\pm T}M_{\tilde{\chi}^{\pm}}\tilde{\psi}^{\pm},
\]
where $\tilde{\psi}^{\pm} = (\tilde{W}^+, \tilde{H}^+_u, \tilde{W}^-, \tilde{H}^-_d)$ and
\[
M_{\tilde{\chi}^{\pm}} = 
\begin{bmatrix} 
0 & 0 & M_2 & gv_d \\
0 & 0 & gv_u & \mu \\
M_2 & gv_u & 0 & 0 \\
gv_d & \mu & 0 & 0
\end{bmatrix}
\]
In this matrix the $M_2$ terms come from the soft terms for the $W^{\pm}$, the $\mu$ terms come from the superpotential and the rest come from the kinetic terms in the Lagrangian.
Same as for the neutralino mass matrix, we can rewrite the terms from the kinetic part in terms of $\beta$ and the mass of the W:
\[
gv_d = \cos\beta m_W
\]
\[
gv_u = \sin\beta m_W
\]
This matrix can also be diagonalized to find the masses of the charginos. The matrix has doubly degenerate eigenvalues giving mass to two Dirac fermions and their antiparticles. 

\section{Parameters}
For the neutralino mass matrix we see that we have four parameters that determine the eigenvalues. These are $M_1$, $M_2$, $\mu$ and $\tan\beta$. For the chargino mass matrix there is only three: $M_2$, $\mu$ and $\tan\beta$. We have written a program in {\tt c++} that can find the eigenvalues for the mass matrices with different values of the parameters as input. This way we can study how the masses of the particles depend upon the different parameters. We will focus on $M_1$, $M_2$ and $\mu$ for the first part of our investigation and keep $\tan\beta$ at a fixed value. The effect of $\tan\beta$ will be considered later in the paper. \\\\
Figures \ref{fig:1} and \ref{fig:2} show the masses of neutralinos and charginos in different regions of the parameter space, both as a function of $M_2$. In figure \ref{fig:1} we see that for $M_2 < M_1 << \mu$, the $\tilde{\chi}^0_1$ and $\tilde{\chi}^{\pm}_1$ are mostly winos (their values are determined by $M_2$), the $\tilde{\chi}^{0}_2$ is mostly a bino (determined by $M_1$) and the $\tilde{\chi}^{0}_3$, $\tilde{\chi}^{0}_4$ and $\tilde{\chi}^{\pm}_2$ are mostly higgsinos (determined by $\mu$). As soon as $M_2$ becomes larger than $M_1$, the two lightest neutralinos ``switch places'' so the $\tilde{\chi}^{0}_1$ becomes a bino, and the $\tilde{\chi}^{0}_2$ becomes a wino. When $M_2$ becomes much larger than $\mu$, the $\tilde{\chi}^{\pm}_1$ and $\tilde{\chi}^{0}_2$ go from winos to higgsinos, and the $\tilde{\chi}^{\pm}_2$ and $\tilde{\chi}^{0}_4$ go from higgsinos to winos.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{masses.pdf}
  \caption{Mass spectrum of neutralinos and charginos as a function of $M_2$.}
  \label{fig:1}
\end{figure}

In figure \ref{fig:2} we see that for $M_2 << \mu < M_1$, the $\tilde{\chi}^{0}_1$ and $\tilde{\chi}^{\pm}_1$ are mostly winos, the $\tilde{\chi}^{0}_2$, $\tilde{\chi}^{0}_3$ and $\tilde{\chi}^{\pm}_2$ are mostly higgsinos, and the $\tilde{\chi}^{0}_4$ is mostly a bino. When $M_2$ becomes much larger than $\mu$, the $\tilde{\chi}^{0}_1$ and $\tilde{\chi}^{\pm}_1$ become higgsinos, and the $\tilde{\chi}^{0}_3$ and $\tilde{\chi}^{\pm}_2$ become winos. When $M_2$ becomes larger than $M_1$, the $\tilde{\chi}^{0}_3$ switches place with the $\tilde{\chi}^{0}_4$ and goes from wino to bino.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{masses2.pdf}
  \caption{Mass spectrum of neutralinos and charginos as a function of $M_2$.}
  \label{fig:2}
\end{figure}

From both figures \ref{fig:1} and \ref{fig:2} we see that there is most mixing between the mass eigenstates when the $\mu$ parameter is close in value to $M_1$ or $M_2$, or both. In the regions where $\mu$ is far from the other parameters in value, there is less mixing between the eigenvalues and the particles are mostly ``pure'' bino, wino and higgsino states. For these regions we see that $M_1$ gives mass to only one neutralino and to no charginos. This is because $M_1$ appears only once in the neutralino mass matrix, $M_{\tilde{\chi}^0}$, and not in the chargino mass matrix, $M_{\tilde{\chi}^{\pm}}$, at all. $M_2$ gives mass to one neutralino and two charginos because it appears once in $M_{\tilde{\chi}^0}$ and twice in $M_{\tilde{\chi}^{\pm}}$. $\mu$ gives mass to two charginos and two neutralinos as it appears twice in both mass matrices. 

When one mass parameter is much smaller than the others, which is what we set out to study, the lightest neutralino is always in a mass state given by this smallest parameter. If $M_1$ is the smallest, then the lightest neutralino is a bino LSP, and is distinctly lighter than the other particles and therefore a good dark matter candidate. If $M_2$ or $\mu$ is the smallest however, the lightest neutralino has approximately the same mass as the lightest chargino and maybe also the second lightest neutralino. These cases could make the lightest chargino the LSP.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{mass(tan(beta)).pdf}
  \caption{Mass spectrum of neutralinos and charginos as a function of $\tan\beta$.}
  \label{fig:beta}
\end{figure}

So far we have kept $\tan\beta$ at a fixed value and only considered the effect of $M_1, M_2$ and $\mu$ on the mass eigenvalues. If we look at the mass matrices we see that $\tan\beta$ is proportional to the mass of the $Z$ or the $W$ boson. The effect this parameter has on the eigenvalues is therefore negligible compared to $M_1, M_2$ and $\mu$ in the regions of the parameter space that we have considered. For $\tan\beta$ to make a noticable difference to the masses, $M_1, M_2$ and $\mu$ need to have values much closer to $m_Z$ and $m_W$. Figure \ref{fig:beta} shows a plot of the mass spectrum for much lower values of $M_1, M_2$ and $\mu$, this time as a function of $\tan\beta$. We see that different values for $\tan\beta$ give different masses to the particles. The effect is especially prominent for low values of $\tan\beta$ where it can change the order of the particles. 

Lastly we wanted to check, for the case where the sum of the two neutralinos is below 90 GeV, how heavy the lightest chargino can be, and in particular if it can be heavier than the sum of the two lightest neutralinos, or heavier than 90 GeV. However, it is clear from figures \ref{fig:1} and \ref{fig:2} that the lightest chargino can not be much heavier than the second lighest neutralino, as it always has approximately the same mass as one of the two lightest neutralinos. The heaviest the lightest chargino can be is approximately 90 GeV if $M_1 << M_2 << \mu$, when the lightest neutralino is a bino with mass close to zero, and the second lightest neutralino is a wino with mass close to 90 GeV, same as the chargino, but it can not be heavier than the sum of the two. The exception is when the values of all the parameters are low, as we can see from figure \ref{fig:beta}. Then all the particles can have distinctly separate mass values, and the chargino can be much heavier than the two lightest neutralinos, but this is only when all the three lightest particles are way below 90 GeV. When the masses get heavier, the lightest chargino approaches one of the two lightest neutralinos in mass and can therefore never be heavier than 90 GeV when the sum of the two neutralinos is equal to or below that.

\section{Conclusion}
We have seen in this paper how the parameters $M_1$, $M_2$, $\mu$ and $\tan\beta$ in the mass matrices for the neutralinos and charginos determine the mass spectrum and neutralino-chargino composition. For regions where the parameters are far apart in value, in particular when $\mu$ is far from the other parameters, there is little mixing and the mass eigenvalues are mostly in pure wino, bino and higgsino states. In these regions $M_1$ gives mass to one neutralino, $M_2$ gives mass to one neutralino and two charginos, and $\mu$ gives mass to two neutralinos and two charginos. The effect of $\tan\beta$ is negligible compared to the other parameters when the other parameters are much larger than $m_Z$. 

When one mass parameter is much smaller than the others (ignoring $\tan\beta$ in this case), this parameter determines the mass of the LSP. If $M_2 << M_1$ and $\mu$, then $\tilde{\chi}^0_1$ is a wino and has approximately the same mass as $\tilde{\chi}^{\pm}_1$. If $\mu << M_1$ and $M_2$, $\tilde{\chi}^0_1$ is a higgsino and has approximately the same mass as $\tilde{\chi}^{\pm}_1$ and $\tilde{\chi}^{0}_2$. If $M_1 << M_2$ and $\mu$, then $\tilde{\chi}^0_1$ is a bino LSP and is distinctly smaller than the other particles which makes it a good candidate for dark matter since it is a stable and neutral particle.

Finally we discovered that if the sum of the two lightest neutralinos is below 90 GeV, the lightest chargino can not be heavier than 90 GeV. This is because it has approximately the same mass as one of the two lightest neutralinos in regions where these masses are not far below 90 GeV.


\begin{thebibliography}{9}
%
%% Please take references when possible from SPIRES
%% http://inspirehep.net/
%
%%\cite{Barr:2013sla}
%\bibitem{Barr:2013sla}
%  A.~J.~Barr and C.~G.~Lester,
%  %``A search for direct heffalon production using the ATLAS and CMS experiments at the Large Hadron Collider,''
%  arXiv:1303.7367 [hep-ph].
%  %%CITATION = ARXIV:1303.7367;%%
%  %1 citations counted in INSPIRE as of 14 Oct 2015
%
%%\cite{Haber:1984rc}
%\bibitem{Haber:1984rc}
%  H.~E.~Haber, G.~L.~Kane,
%  %``The Search for Supersymmetry: Probing Physics Beyond the Standard Model,''
%  Phys.\ Rept.\  {\bf 117 } (1985)  75-263.
%  
%\cite{Richardson:2000nt}
%\bibitem{Richardson:2000nt}
%  P.~Richardson,
%  %``Simulations of R-parity violating SUSY models,''
%  hep-ph/0101105.
%  %%CITATION = HEP-PH/0101105;%%
%  %20 citations counted in INSPIRE as of 29 Oct 2013
  
%\cite{Martin:1997ns}
\bibitem{Martin:1997ns}
  S.~P.~Martin,
  %``A Supersymmetry primer,''
  Adv.\ Ser.\ Direct.\ High Energy Phys.\  {\bf 21} (2010) 1
   [Adv.\ Ser.\ Direct.\ High Energy Phys.\  {\bf 18} (1998) 1]
  %doi:10.1142/9789812839657_0001, 10.1142/9789814307505_0001
  [hep-ph/9709356].
  %%CITATION = doi:10.1142/9789812839657_0001, 10.1142/9789814307505_0001;%%
  %2517 citations counted in INSPIRE as of 30 Nov 2015

\end{thebibliography}

\end{document}
